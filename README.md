# Cheating the Extraterrestrials

"Hey Dave, I know how bad you are at games so I put in some cheats in for you."

You are an astronaut who has been kidnapped by aliens! Fortunately, the aliens thought it would be a good idea to lock you in the armory; unfortunately, the aliens have never heard of doors so you have no way of getting from room to room. Or do you?

Originally made for Major Jam 4: Cosmic.  
https://infinity48.itch.io/cheating-the-extraterrestrials

You can submit PRs and issues if you want to help me find bugs or something.

## Building

Things needed:
>Godot 3.4 or later  
The source code for the game

1. Download the source code for the game.
2. Open the game in Godot.
3. Go to Project -> Export
4. Setup the export template(s).
5. Export the project.
