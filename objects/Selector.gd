# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
tool
extends Control
class_name Selector

export var line_gap := Vector2(0, 20)
export var text_centered := false

var _font = get_font('font')
var selection := 0
var choices := ['Foo', 'Bar', 'Baz', 'Mah']
signal selected(selection)

func _ready():
	if !Engine.editor_hint:
		choices = []

func _process(delta):
	update()
	if visible and !Engine.editor_hint:
		if Input.is_action_just_pressed('ui_up'):
			if selection <= 0:
				selection = len(choices) - 1
			else:
				selection -= 1
		elif Input.is_action_just_pressed('ui_down'):
			if selection >= len(choices) - 1:
				selection = 0
			else:
				selection += 1
		elif Input.is_action_just_pressed('ui_accept'):
			emit_signal('selected', selection)

func _draw():
	var colour
	var lines
	for i in range(len(choices)):
		if i == selection:
			colour = Color.yellow
		else:
			colour = Color.white
		lines = choices[i].split('\n')
		for o in range(len(lines)):
			var pos = line_gap * i
			if text_centered: pos.x += _font.get_string_size(lines[o]).x / 2
			pos.y += _font.get_string_size(lines[o]).y * o
			draw_string(_font, pos, lines[o], colour)
