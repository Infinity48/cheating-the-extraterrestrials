# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends CanvasLayer

const SAVE_FILE = "user://cheating_the_extraterrestrials.cfg"
const VERSION := {
	"major": 2, 
	"minor": 0, 
	"patch": 0, 
	"status": "dev1", 
}

signal last_enemy_killed

var config := ConfigFile.new()
var time := 0.0
var best_time := INF
var aliens setget set_aliens, get_aliens
var level := ""
var just_died := false
var _allkills_sigdone := false
var _aliens := 0
onready var gui := $GUI
onready var healthbar := $GUI/HealthBar
onready var music := $Music

func _ready():
	set_process(false)
	
	if !load_config():
		print("NO SAVE!")
		# Migrate old save file.
		var dir = Directory.new()
		if dir.rename("user://game.cfg", SAVE_FILE) == OK:
			print("Migrating old save...")
			load_config()

func _process(delta):
	time += delta
	$GUI/Aliens.text = "Aliens left: %s"%_aliens
	$GUI/Time.text = "Time: %s:%02d"%[int(time) / 60, int(time) % 60]
	if !_allkills_sigdone and _aliens <= 0 and !just_died:
		emit_signal("last_enemy_killed")
		_allkills_sigdone = true

func _input(event):
	if $Quit.visible:
		if event.is_action_pressed("ui_cancel"):
			# Reset game to the initial state at launch.
			load_config()
			level = ""
			time = 0.0
			_aliens = 0
			set_process(false)
			gui.visible = false
			music.stop()
			$Quit.visible = false
			get_tree().paused = false
			get_tree().change_scene("res://levels/Title.tscn")
		elif event.is_action_pressed("ui_accept") or event is InputEventMouseButton:
			$Quit.visible = false
			get_tree().paused = false
			set_process(true)
	elif event.is_action_pressed("ui_cancel"):
		pause()
	elif event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
		config.set_value("Other", "fullscreen", OS.window_fullscreen)
		config.save(SAVE_FILE)
	elif event.is_action_pressed("screenshot"):
		var image = get_viewport().get_texture().get_data()
		image.flip_y()
		image.save_png("user://screenshot.png")

func _notification(what):
	match what:
		NOTIFICATION_WM_QUIT_REQUEST:
			if !$Quit.visible:
				pause()
			else:
				quit()

func save():
	if !level:
		return
	config.set_value("Save", "time", time)
	config.set_value("Save", "level", level)
	config.save(SAVE_FILE)

func save_highscore(time):
	if time < best_time:
		config.set_value("Other", "best_time", time)
		config.save(SAVE_FILE)

func load():
	if config.load(SAVE_FILE) == OK:
		time = config.get_value("Save", "time")
		level = config.get_value("Save", "level")
		music.play()
		$GUI.visible = true
		set_process(true)
		get_tree().change_scene(level)

func load_config() -> bool:
	if config.load(SAVE_FILE) == OK:
		best_time = config.get_value("Other", "best_time", INF)
		OS.window_fullscreen = config.get_value("Other", "fullscreen", false)
		return true
	return false

func get_player() -> Player:
	return get_tree().get_nodes_in_group("Player")[0]

func get_aliens():
	return _aliens

func set_aliens(val):
	_aliens = val
	_allkills_sigdone = false

func get_version_string():
	var ret = "{major}.{minor}.{patch}".format(VERSION)
	if VERSION.status != "stable":
		ret += "-" + VERSION.status
	return ret

func quit():
	if OS.has_feature("pc"):
		get_tree().quit(0)

func pause():
	var scn = get_tree().current_scene.name
	if scn == "Title":
		quit()
		return
	elif scn == "ModManagerUI":
		get_tree().change_scene("res://levels/Title.tscn")
		return
	$Quit.visible = true
	get_tree().paused = true
	set_process(false)
