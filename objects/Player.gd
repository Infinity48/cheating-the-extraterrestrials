# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
class_name Player
extends KinematicBody2D

export var speed := 160

enum key {UP, DOWN, LEFT, RIGHT}

var velocity := Vector2()
var health := 5
var inv := false
var cheat := []
onready var _sprite := $CollisionShape/Sprite

func _ready():
	randomize()
	GameController.just_died = false

func _process(delta):
	GameController.healthbar.value = health
	
	# Controls
	velocity = Input.get_vector("move_left", "move_right", "move_up", "move_down") * speed
	if velocity.x > 0:
		_sprite.flip_h = true
	elif velocity.x < 0:
		_sprite.flip_h = false
	
	if Input.is_action_just_pressed("shoot"):
		var bul = load("res://objects/Bullet.tscn").instance()
		bul.maker = self
		bul.position = position
		bul.target = get_global_mouse_position()
		get_parent().add_child(bul)
	
	match cheat:
		[key.UP, key.DOWN, key.UP, key.RIGHT]:
			position.x += 64
			cheat = []
		[key.UP, key.DOWN, key.UP, key.LEFT]:
			position.x -= 64
			cheat = []

func _input(event):
	if len(cheat) > 4:
		cheat = []
	if event.is_action_pressed("cheat_up"):
		add_cheat(key.UP)
	elif event.is_action_pressed("cheat_down"):
		add_cheat(key.DOWN)
	elif event.is_action_pressed("cheat_left"):
		add_cheat(key.LEFT)
	elif event.is_action_pressed("cheat_right"):
		add_cheat(key.RIGHT)

func _physics_process(delta):
	velocity = move_and_slide(velocity)

func add_cheat(key):
	cheat.append(key)
	$CheatTimer.start(0.75)

func hurt(damage):
	if inv:
		return
	var hurt_sound = get_node("HurtSound" + str(randi() % 3))
	hurt_sound.play()
	health -= damage
	inv = true
	_sprite.modulate.a = 0.5
	$HurtTimer.start(2.0)
	if health <= 0:
		get_tree().paused = true
		yield(hurt_sound, "finished")
		get_tree().paused = false
		GameController.just_died = true
		GameController.aliens = 0
		get_tree().reload_current_scene()

func _on_Enemy_touched(toucher, damage):
	if toucher == self:
		hurt(damage)

func _on_HurtTimer_timeout():
	inv = false
	_sprite.modulate.a = 1.0

func _on_CheatTimer_timeout():
	cheat = []

