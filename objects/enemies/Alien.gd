# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Enemy

var _can_see := false

func _ready():
	# call_group() can't operate on children.
	for enemy in get_tree().get_nodes_in_group("Enemies"):
		enemy.get_node("RayCast").add_exception(self)

func _process(delta):
	$RayCast.cast_to = to_local(_player.global_position)
	_can_see = $RayCast.get_collider() == _player

func _physics_process(delta):
	if _can_see:
		var angle = get_angle_to(_player.position)
	
		velocity.x = cos(angle) * speed
		velocity.y = sin(angle) * speed
	velocity = move_and_slide(velocity)
