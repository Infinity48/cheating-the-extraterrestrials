# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends "res://objects/enemies/Alien.gd"

var dieing := false

func _ready():
	randomize()

func _physics_process(delta):
	if !dieing:
		._physics_process(delta)

func kill():
	if health <= 1:
		var anim = _DEATH_ANIM.instance()
		anim.speed = 0.2
		anim.scale = scale
		anim.texture = $CollisionShape/Sprite.texture
		anim.position = position + $CollisionShape/Sprite.position
		dieing = true
		velocity = Vector2.ZERO
		GameController.aliens -= 1
		$DeathSound.play()
		$Hitbox.queue_free()
		$CollisionShape.queue_free()
		get_parent().add_child(anim)
	else:
		.kill()

func _on_BulletDelay_timeout():
	var bul = preload("res://objects/Bullet.tscn").instance()
	bul.maker = self
	bul.position = position
	bul.target = _player.position
	get_parent().add_child(bul)
