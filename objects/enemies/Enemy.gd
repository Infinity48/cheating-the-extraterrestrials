# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
class_name Enemy
extends KinematicBody2D

export var speed := 60
export var attack := 1
export var inital_health := 1
const _DEATH_ANIM := preload("res://objects/DeathAnim.tscn")

var velocity := Vector2()
var inv := false
onready var _player := GameController.get_player()
onready var health := inital_health

signal touched(toucher, damage)

func _ready():
	add_collision_exception_with(_player)
	connect("touched", _player, "_on_Enemy_touched")
	GameController.aliens += 1

func _process(delta):
	if has_node("Hitbox"):
		for body in $Hitbox.get_overlapping_bodies():
			emit_signal("touched", body, attack)

func kill() -> void:
	if inv:
		return
	$DeathSound.play()
	if health >= 2:
		inv = true
		modulate.a = 0.5
		yield($DeathSound, "finished")
		inv = false
		modulate.a = 1
		health -= 1
	else:
		GameController.aliens -= 1
		$DeathSound.play()
		$Hitbox.queue_free()
		var anim = _DEATH_ANIM.instance()
		anim.texture = $CollisionShape/Sprite.texture
		anim.position = $CollisionShape.position
		add_child(anim)
		$CollisionShape/Sprite.queue_free()
		yield($DeathSound, "finished")
		queue_free()
