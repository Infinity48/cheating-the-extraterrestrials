# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Camera2D

onready var _player := GameController.get_player()

func _ready():
	# Is the setting overwritten?
	if !smoothing_enabled:
		# Defaults
		smoothing_enabled = true
		smoothing_speed = 8
	if !drag_margin_h_enabled and !drag_margin_v_enabled:
		drag_margin_h_enabled = true
		drag_margin_v_enabled = true
		drag_margin_left = 0.1
		drag_margin_top = 0.1
		drag_margin_right = 0.1
		drag_margin_bottom = 0.1

func _process(delta):
	position = _player.position
