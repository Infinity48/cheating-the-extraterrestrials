# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends TileMap

func _ready():
	for tile in get_used_cells():
		if get_cellv(tile) == 3:
			var obj = preload("res://objects/Healthkit.tscn").instance()
			obj.position = map_to_world(tile)
			add_child(obj)
			set_cellv(tile, -1)
		elif get_cellv(tile) == 4:
			var obj = preload("res://objects/Window.tscn").instance()
			obj.position = map_to_world(tile)
			add_child(obj)
			set_cellv(tile, -1)
