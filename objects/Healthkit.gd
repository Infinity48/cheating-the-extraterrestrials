# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Sprite

func _on_body_entered(body):
	if body is Player:
		if body.health < 5:
			body.health += 1
			visible = false
			$Area.queue_free()
			$Collect.play()
			yield($Collect, "finished")
			queue_free()
