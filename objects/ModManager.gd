# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Node

var mod_info := {}
var _mod_count := 0
var dir = "mods/"

func _ready():
	if OS.has_feature("web"):
		dir = "user://cheating_the_extraterrestrials_mods/"
	elif OS.has_feature("standalone"):
		dir = OS.get_executable_path().get_base_dir() + "/mods/"
	
	var success = load_mod_info()
	if !success:
		var directory = Directory.new()
		print("Trying to save new file...")
		if !directory.dir_exists(dir):
			directory.make_dir(dir)
		save_mod_info()
	reload_all_mods()

func get_mod_count():
	return _mod_count

func load_mod_info() -> bool:
	var file = File.new()
	var err = file.open(dir + "mods.json", File.READ)
	if err != OK:
		printerr("Error loading mods.json.")
		file.close()
		return false
	mod_info = parse_json(file.get_as_text())
	file.close()
	return true

func save_mod_info():
	var file = File.new()
	file.open(dir + "mods.json", File.WRITE)
	file.store_string(to_json(mod_info))
	file.close()

func load_mod(name: String) -> bool:
	var entry = mod_info[name]
	var filename = name + "." + entry["fileType"]
	var replace = entry["replaceAssets"]
	var success = ProjectSettings.load_resource_pack(dir + filename, replace)
	if !success:
		printerr("Error loading mod: " + name)
	else:
		print("Loaded mod: " + name)
		_mod_count += 1
	return success

func add_mod(filename: String) -> String:
	var filetype
	if filename.ends_with(".zip"):
		filename = filename.trim_suffix(".zip")
		filetype = "zip"
	else:
		filename = filename.trim_suffix(".pck")
		filetype = "pck"
	
	mod_info[filename] = {"displayName": "",
							"enabled": true,
							"fileType": filetype, 
							"replaceAssets": false}
	var mod_keys = mod_info.keys()
	var idx = mod_keys.find(filename)
	mod_info[filename]["displayName"] = "New Mod %s"%idx
	return filename

func reload_all_mods() -> bool:
	_mod_count = 0
	var success = true
	for mod in mod_info.keys():
		if mod_info[mod]["enabled"]:
			success = load_mod(mod)
	return success
