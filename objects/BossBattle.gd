# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends AnimationPlayer

var _death_start := false

func _ready():
	play("BossAppear")

func _process(delta):
	if $Boss.dieing and !_death_start:
		play("BossDie")
		GameController.music.stop()
		GameController.gui.visible = false
		_death_start = true

func _on_animation_finished(anim_name):
	if anim_name == "BossDie":
		get_tree().change_scene("res://levels/Ending.tscn")
	$"../Camera".current = true

func _on_animation_started(anim_name):
	var anim = get_animation(anim_name)
	var key = anim.track_find_key(0, 0)
	anim.track_set_key_value(0, key, GameController.get_player().position)
	if anim_name == "BossDie":
		key = anim.track_find_key(0, 1)
		anim.track_set_key_value(0, key, $Boss.position)
