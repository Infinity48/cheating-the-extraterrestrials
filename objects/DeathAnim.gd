# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Sprite

var _progress := 1.0
var speed := 1.6

func _process(delta):
	_progress -= delta * speed
	if _progress <= 0.0:
		queue_free()
	material.set_shader_param('value', _progress)
