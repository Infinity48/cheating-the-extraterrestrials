# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends KinematicBody2D

const OUTSIDE_DIST = 128

var velocity := Vector2()
var target := Vector2()
var maker:Node
var _level_rect:Rect2

func _ready():
	var cam = get_tree().current_scene.get_node("Camera")
	_level_rect = Rect2(cam.limit_left,
						cam.limit_top,
						(cam.limit_right - cam.limit_left),
						(cam.limit_bottom - cam.limit_top))
	
	var angle = get_angle_to(target)
	velocity.x = cos(angle) * 360
	velocity.y = sin(angle) * 360
	rotation_degrees = rad2deg(angle) - 90
	
	if !(maker is Player):
		$Sprite.texture = preload("res://images/enemybul.png")

func _physics_process(delta):
	velocity = move_and_slide(velocity)
	# Are we outside of the level?
	if (position.x < _level_rect.position.x - OUTSIDE_DIST 
	or  position.x > _level_rect.end.x + OUTSIDE_DIST
	or  position.y < _level_rect.position.y - OUTSIDE_DIST
	or  position.y > _level_rect.end.y + OUTSIDE_DIST):
		queue_free()

func destroy():
	if $Sound.playing:
		visible = false
		$Area.queue_free()
		yield($Sound, "finished")
	queue_free()

func _on_area_entered(area):
	var obj = area.get_parent()
	if obj.has_method("kill") and obj != maker:
		obj.kill()
		destroy()

func _on_body_entered(body):
	if body is TileMap:
		destroy()
	elif body is Player and !(maker is Player):
		body.hurt(1)
		destroy()
