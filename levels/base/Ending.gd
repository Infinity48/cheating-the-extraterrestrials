# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Node2D

var _flash := 0
var _explo_type := ""
onready var _alarm := $Overlay/Alarm
onready var _explo := $Overlay/Explo

func _process(delta):
	if _alarm.color.a >= 0.8:
		_flash = 1
	elif _alarm.color.a <= 0:
		_flash = 0
	
	if _flash == 0:
		_alarm.color.a += delta
	else:
		_alarm.color.a -= delta
	
	if _explo_type:
		_explo.color.a += delta
		if _explo.color.a >= 1.0:
			var time = GameController.time
			GameController.save_highscore(time)
			$Ending/Credits.text = $Ending/Credits.text%[int(time) / 60, int(time) % 60]
			$Ending.play(_explo_type)
			
			# Clean up so pausing and unpausing doesn't break ending.
			for obj in get_children():
				if obj != $Ending:
					obj.queue_free()
			set_process(false)

func _on_Exit_body_entered(body):
	if !body is TileMap:
		_explo_type = "Win"

func _on_Timer_timeout():
	_explo_type = "Lose"
