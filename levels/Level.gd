extends Node2D

export(String, FILE, "*.tscn,*.scn") var next_level := ""

func _ready():
	GameController.connect("last_enemy_killed", self, "_on_last_enemy_killed")

func _on_last_enemy_killed():
	if next_level:
		GameController.level = next_level
		GameController.save()
		get_tree().change_scene(next_level)
