# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends ColorRect

func _ready():
	$Selector.choices = ["Continue", "New Game", "Mod Manager", "Quit"]
	if !OS.has_feature("pc"):
		$Selector.choices.remove(3)
	var time = GameController.best_time
	$BestTime.text = $BestTime.text%[int(time) / 60, int(time) % 60]
	$BestTime.visible = GameController.best_time != INF
	$Version.text = GameController.get_version_string()

func _on_selected(selection):
	match selection:
		0:
			GameController.load()
		1:
			GameController.music.play()
			GameController.gui.visible = true
			GameController.set_process(true)
			get_tree().change_scene("res://levels/base/0.tscn")
		2:
			get_tree().change_scene("res://levels/ModManagerUI.tscn")
		3:
			GameController.quit()
