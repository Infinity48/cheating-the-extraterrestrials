extends "res://levels/Level.gd"

const BOSS := preload("res://objects/BossBattle.tscn")

func _on_last_enemy_killed():
	if !has_node("BossBattle"):
		add_child(BOSS.instance())
