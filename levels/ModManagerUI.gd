# Cheating The Extraterrestrials
# Copyright (C) 2022 Infinity48
# 
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
extends Panel

const ICON_DISABLED = preload("res://images/UI-Icon_Disabled.tres")
const ICON_ENABLED = preload("res://images/UI-Icon_Enabled.tres")

var _mod_entry := {}
var _mod_selected := ""
var _html5_file_cb
var _html5_file_open

onready var _mod_keys := ModManager.mod_info.keys()
onready var _name_box := get_node("%NameBox")
onready var _enable_check := get_node("%EnableCheck")
onready var _replace_check := get_node("%ReplaceCheck")
onready var _apply_button := get_node("%ApplyButton")
onready var _revert_button := get_node("%RevertButton")
onready var _remove_button := get_node("%RemoveButton")

func _ready():
	if OS.has_feature("web"):
		# HORRIBLE KLUGE: This mess is the only way I could find to upload mods on HTML5.
		_html5_file_cb = JavaScript.create_callback(self, "_on_html5_AddDialog_selected")
		JavaScript.eval("""
			window.openFileDialog = function (callback) {
				const fileSelector = document.createElement('input');
				fileSelector.type = 'file';
				fileSelector.accept = '.zip, .pck'
				fileSelector.style.position = 'absolute';
				fileSelector.style.opacity = '0';
				document.body.appendChild(fileSelector);
				
				fileSelector.addEventListener('change', (event) => {
					const selector = event.target;
					const file = selector.files[0];
					var reader = new FileReader();
					reader.readAsArrayBuffer(file);
					reader.addEventListener('loadend', (event) => {
						callback(file.name, reader.result);
					});
					selector.remove();
				});
				
				fileSelector.click();
			}
		""", true)
		_html5_file_open = JavaScript.get_interface("window")
	setup_modlist()

func setup_modlist():
	$ModList.clear()
	for key in _mod_keys:
		var entry = ModManager.mod_info[key]
		if entry["enabled"]:
			$ModList.add_item(entry["displayName"], ICON_ENABLED)
		else:
			$ModList.add_item(entry["displayName"], ICON_DISABLED)

func setup_editor(entry, key):
	_mod_entry = entry
	_mod_selected = key
	_name_box.text = entry["displayName"]
	_name_box.editable = true
	_enable_check.pressed = entry["enabled"]
	_enable_check.disabled = false
	_replace_check.pressed = entry["replaceAssets"]
	_replace_check.disabled = false
	
	_apply_button.disabled = false
	_revert_button.disabled = false
	_remove_button.disabled = false

func _on_ExitButton_pressed():
	get_tree().change_scene("res://levels/Title.tscn")
	
func _on_AddButton_pressed():
	if OS.has_feature("web"):
		_html5_file_open.openFileDialog(_html5_file_cb)
	else:
		$AddDialog.current_path = ModManager.dir
		$AddDialog.show()
		$AddDialog.invalidate() # I need to do this or else the the folder view won't display properly.

func _on_AddDialog_file_selected(path: String):
	var filename = path.trim_prefix(path.get_base_dir() + "/")
	print(filename)
	var mod_key = ""
	var idx = -1
	
	# WARNING! This only works proberly on exported builds!
	# This is needed as, on Windows at least, copying a file to itself can corrupt it...
	# so this is a workaround for OS-level bug, I guess.
	if !path.begins_with(ModManager.dir):
		var dir = Directory.new()
		print(path)
		dir.copy(path, ModManager.dir + "/" + filename)
	
	mod_key = ModManager.add_mod(filename)
	_mod_keys = ModManager.mod_info.keys()
	
	idx = _mod_keys.find(mod_key)
	setup_modlist()
	$ModList.select(idx)
	_on_ModList_item_selected(idx)
	_revert_button.disabled = true

func _on_html5_AddDialog_selected(args):
	var name = args[0]
	var data = args[1]
	var file = File.new()
	var mod_key = ""
	var idx = -1
	
	file.open(ModManager.dir + name, File.WRITE)
	var bytes = JavaScript.create_object("Int8Array", data)
	for i in range(bytes.length - 1):
		file.store_8(bytes[i])
	file.close()

	mod_key = ModManager.add_mod(name)
	_mod_keys = ModManager.mod_info.keys()

	idx = _mod_keys.find(mod_key)
	setup_modlist()
	$ModList.select(idx)
	_on_ModList_item_selected(idx)
	_revert_button.disabled = true

func _on_ModList_item_selected(index):
	var key = _mod_keys[index]
	setup_editor(ModManager.mod_info[key], key)

func _on_RevertButton_pressed():
	var key = _mod_selected
	setup_editor(ModManager.mod_info[key], key)

func _on_ApplyButton_pressed():
	_mod_entry["displayName"] = _name_box.text
	_mod_entry["enabled"] = _enable_check.pressed
	_mod_entry["replaceAssets"] = _replace_check.pressed
	ModManager.save_mod_info()
	
	_name_box.editable = false
	_enable_check.disabled = true
	_replace_check.disabled = true
	_apply_button.disabled = true
	_revert_button.disabled = true
	_remove_button.disabled = true
	
	setup_modlist()
	$RestartPrompt.show_modal(true)

func _on_RemoveButton_pressed():
	$RemovePrompt.show_modal(true)

func _on_RestartPrompt_confirmed():
	# Restart the game.
	GameController.quit()
	if OS.has_feature("web"):
		JavaScript.eval("window.location.reload();")
	else:
		OS.execute(OS.get_executable_path(), [], false)

func _on_RemovePrompt_confirmed():
	_name_box.editable = false
	_enable_check.disabled = true
	_replace_check.disabled = true
	_apply_button.disabled = true
	_revert_button.disabled = true
	_remove_button.disabled = true
	
	# We don't want to keep discarded mods in browser storage.
	if OS.has_feature("web"):
		var path = ModManager.dir + _mod_selected + "." + _mod_entry["fileType"]
		var dir = Directory.new()
		dir.remove(path)
	
	ModManager.mod_info.erase(_mod_selected)
	ModManager.save_mod_info()
	_mod_keys = ModManager.mod_info.keys()
	setup_modlist()
	$RestartPrompt.show_modal(true)
