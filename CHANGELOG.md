# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] 2022-11-19
### Added
- A modding system.
- Smooth camera.
### Changed
- Windows can now be fired through.
- Controls are now slightly more responsive.
- Minor level design changes.

## [1.1.1] 2022-08-03
### Fixed
- Pressing escape on title screen no longer causes the game to crash.
- Background now shows up properly on ending.
- Pausing and unpausing during ending cutscene no longer breaks.

## [1.1.0] 2022-07-28
### Added
- A background image.
- Healthkits now make a sound.
### Removed
- Got rid of original level 7.
### Changed
- Quitting now returns to title on desktop.
- Level layouts were changed.
- Game now only has 9 levels instead of 10.
### Fixed
- Quitting to title and starting a new game no longer keeps the same time and alien count as before you quit.

## [1.0.1] 2022-07-19
### Fixed
- HTML5 version no longer crashes when quiting.

## [1.0.0] 2022-07-19
### Added
- A save system.
- New levels.
- New alien types.
- Aliens have a death animation now.
- A boss battle and an actual ending.
### Changed
- Remade game in Godot.
- Minor level layout changes.
- Bullets are destroyed after shooting an alien.
- You have more health.

## [0.2.0] 2021-08-23
### Added
- Heath packs.
- You are now told how long it took you to beat the game at the end of it.
### Changed
- Some graphics.
- The health counter is now a health bar.

## [0.1.0] 2021-08-12
### Added
- Everything. Orginal jam version.
